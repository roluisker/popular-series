# popular-series

1. Las capas de la aplicación (por ejemplo capa de persistencia, vistas, red, negocio, etc) y qué clases pertenecen a cual.

 Single Activity principle

 La aplicacion tiene una arquitetura MVVM la cual busca dividir responsabilidades:

 View -> Capa visual, botones, fragments etc..
 ViewModel -> Capa dedicada a la logica del negocio, por ejemplo.. los filtros de busqueda estarian en esta capa y se enviara a la vista los objetos filtrados
 Repository ->  Capa dedicada a la gestion de datos ya sea online o offline

 la carpeta de di -> esta dedicada a la configuracion de dagger y es la encargada de proveer a los modelos las inyecciones
 necesarias

2. La responsabilidad de cada clase creada.

   En general cada paquete de vista contiene:

   Un fragment encargado de la parte visual
   Un modulo de Dagger en donde se indica que se quiere inyectar en el fragment
   Un ViewModel encargado de interactuar con la capa de vista(Fragment)


3. En qué consiste el principio de responsabilidad única? Cuál es su propósito?

   En general cada parte de nuestro codigo debe tener UNA sola tarea logica.
   Por ejemplo si nuestra clase debe hacer carros, no debe por que tener logica para crear una puerta de un bus

4. Qué características tiene, según su opinión, un “buen” código o código limpio?

   SOLID



