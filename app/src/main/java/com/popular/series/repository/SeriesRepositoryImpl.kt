package com.popular.series.repository

import com.google.gson.Gson
import com.google.gson.JsonElement
import com.popular.series.annotation.LATEST_SERIES
import com.popular.series.annotation.POPULAR_SERIES
import com.popular.series.annotation.SerieRequest
import com.popular.series.annotation.TOP_RATED_SERIES
import com.popular.series.api.TvApi
import com.popular.series.db.ReviewDao
import com.popular.series.db.SerieDao
import com.popular.series.interfaces.Serie
import com.popular.series.model.Review
import com.popular.series.model.TvSerie
import com.popular.series.model.Video
import com.popular.series.model.remote.ReviewRemote
import com.popular.series.model.remote.SerieRemote
import com.popular.series.model.remote.VideoRemote
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

class SeriesRepositoryImpl(
    private val serieApi: TvApi,
    private val serieDao: SerieDao,
    private val reviewDao: ReviewDao
) : SeriesRepository {

    override fun series(@SerieRequest request: String): Observable<List<Serie>> {
        when (request) {
            POPULAR_SERIES -> return popularSeriesFromApi(POPULAR_SERIES) as Observable<List<Serie>>
            TOP_RATED_SERIES -> return topRatedSeriesFromApi(TOP_RATED_SERIES) as Observable<List<Serie>>
            LATEST_SERIES -> return latestSeriesFromApi(LATEST_SERIES) as Observable<List<Serie>>
        }
        return Observable.just(emptyList())
    }

    override fun reviews(serieId: String, @SerieRequest request: String): Observable<List<Review>> =
        reviewsFromApi(serieId).onErrorResumeNext(
            reviewFromBd(serieId)
        )

    private fun deserializer(element: Observable<JsonElement>, serieTag: String): Any {
        return element.map { response ->
            Gson().fromJson(response, SerieRemote::class.java).results
        }.doOnNext { storeSeries(it, serieTag) }
    }

    private fun storeSeries(series: List<TvSerie>, tagSerie: String) {
        series.forEach { it.tag = tagSerie }
        Observable.fromCallable { serieDao.insertSeries(series) }
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .doOnError { Timber.e(it, "Series local error") }
            .doOnNext { Timber.i("${series.size} series updated!") }
            .subscribe()
    }

    private fun reviewFromBd(serieId: String): Observable<List<Review>> {
        return reviewDao.reviewsById(serieId).toObservable()
            .doOnNext {
                when {
                    it.isNotEmpty() -> it
                    else -> Observable.just(emptyList<TvSerie>())
                }
            }
    }

    private fun seriesFromBd(tagSerie: String): Observable<List<TvSerie>> {
        return serieDao.seriesByTag(tagSerie).toObservable()
            .doOnNext {
                when {
                    it.isNotEmpty() -> it
                    else -> Observable.just(emptyList<TvSerie>())
                }
            }
    }

    override fun serie(serieId: String): Observable<TvSerie> {
        return serieFromApi(serieId).onErrorResumeNext(serieFromBd(serieId))
    }

    private fun serieFromBd(serieId: String): Observable<TvSerie> {
        return serieDao.serie(serieId).filter { it != null }
            .toObservable()
            .doOnNext {
                Timber.d("Dispatching serie ${it.id} DB...")
            }
    }

    override fun serieVideo(serieId: String): Observable<Video> {
        return serieApi.getVideos(serieId).map { response ->
            var list = Gson().fromJson(response, VideoRemote::class.java).results
            list[0]
        }
    }

    private fun serieFromApi(serieId: String): Observable<TvSerie> {
        return serieApi.getSerie(serieId).map { response ->
            Gson().fromJson(response, TvSerie::class.java)
        }.doOnNext { storeSerie(it) }
    }

    private fun storeSerie(serie: TvSerie) {
        Observable.fromCallable { serieDao.insertSerie(serie) }
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .doOnError { Timber.e(it, "Unable to store") }
            .doOnNext { Timber.i("${serie.id} Serie ") }
            .subscribe()
    }

    private fun storeReviews(reviews: List<Review>) {
        Observable.fromCallable { reviewDao.insertReview(reviews) }
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .doOnError { Timber.e(it, "Unable to store") }
            .doOnNext { Timber.i("Reviews") }
            .subscribe()
    }

    private fun latestSeriesFromApi(tagSerie: String): Any {
        return (deserializer(serieApi.getLatestSeries(), tagSerie) as Observable<List<TvSerie>>).onErrorResumeNext(
            seriesFromBd(tagSerie)
        )
    }

    private fun topRatedSeriesFromApi(tagSerie: String): Any {
        return (deserializer(serieApi.getTopRatedTvSeries(), tagSerie) as Observable<List<TvSerie>>).onErrorResumeNext(
            seriesFromBd(tagSerie)
        )
    }

    private fun popularSeriesFromApi(tagSerie: String): Any {
        return (deserializer(serieApi.getPopularTvSeries(), tagSerie) as Observable<List<TvSerie>>).onErrorResumeNext(
            seriesFromBd(tagSerie)
        )
    }

    private fun reviewsFromApi(serieId: String): Observable<List<Review>> {
        return serieApi.getReviews(serieId).map { response ->
            Gson().fromJson(response, ReviewRemote::class.java).results
        }.doOnNext { storeReviews(it) }
    }

}