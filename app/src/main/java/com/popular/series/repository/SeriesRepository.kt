package com.popular.series.repository

import com.popular.series.annotation.SerieRequest
import com.popular.series.interfaces.Serie
import com.popular.series.model.Review
import com.popular.series.model.TvSerie
import com.popular.series.model.Video
import io.reactivex.Observable

interface SeriesRepository {
    fun series(@SerieRequest request: String): Observable<List<Serie>>
    fun serie(serieId: String): Observable<TvSerie>
    fun serieVideo(serieId: String): Observable<Video>
    fun reviews(serieId: String, @SerieRequest request: String): Observable<List<Review>>
}