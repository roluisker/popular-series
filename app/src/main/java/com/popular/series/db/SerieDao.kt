package com.popular.series.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.popular.series.model.TvSerie
import io.reactivex.Single

@Dao
interface SerieDao {

    @Query("SELECT * FROM TvSerie WHERE id = :serieId")
    fun serie(serieId: String): Single<TvSerie>

    @Query("SELECT * FROM TvSerie")
    fun series(): Single<List<TvSerie>>

    @Query("SELECT * FROM TvSerie WHERE tag = :tagSerie")
    fun seriesByTag(tagSerie: String): Single<List<TvSerie>>

    @Query("DELETE FROM TvSerie WHERE id = :serieId")
    fun deleteSerie(serieId: String): Int

    @Query("DELETE FROM TvSerie")
    fun dropSeries(): Int

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertSeries(movies: List<TvSerie>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertSerie(movies: TvSerie)

}