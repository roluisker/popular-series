package com.popular.series.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.popular.series.model.Review
import com.popular.series.model.TvSerie

@Database(entities = [TvSerie::class, Review::class], version = 6, exportSchema = false)
abstract class TvSeriesDataBase : RoomDatabase() {
    abstract fun serieDao(): SerieDao
    abstract fun reviewDao(): ReviewDao
}