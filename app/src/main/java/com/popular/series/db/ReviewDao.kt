package com.popular.series.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.popular.series.model.Review
import io.reactivex.Single

@Dao
interface ReviewDao {

    @Query("SELECT * FROM Review WHERE id = :reviewId")
    fun review(reviewId: String): Single<Review>

    @Query("SELECT * FROM Review")
    fun reviews(): Single<List<Review>>

    @Query("SELECT * FROM Review WHERE tag = :reviewId")
    fun reviewsById(reviewId: String): Single<List<Review>>

    @Query("DELETE FROM Review WHERE id = :reviewId")
    fun deleteReview(reviewId: String): Int

    @Query("DELETE FROM Review")
    fun dropReview(): Int

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertReview(reviews: List<Review>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertReview(review: Review)

}