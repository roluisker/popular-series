package com.popular.series

import android.app.Application
import androidx.fragment.app.Fragment
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import timber.log.Timber
import javax.inject.Inject

class MastroianniApp : Application(), HasSupportFragmentInjector {

    @Inject
    lateinit var fragmentInjector: DispatchingAndroidInjector<Fragment>

    lateinit var component: MastroianniAppComponent

    override fun onCreate() {
        super.onCreate()
        BuildConfig.DEBUG.takeIf { true }.apply { Timber.plant(Timber.DebugTree()) }
        component = buildDagger()
        component!!.inject(this)
    }

    private fun buildDagger(): MastroianniAppComponent {
        return DaggerMastroianniAppComponent.builder()
            .application(this)
            .context(this)
            .build()
    }

    override fun supportFragmentInjector() = fragmentInjector

}