package com.popular.series

import com.popular.series.adapter.TvSeriesAdapter
import com.popular.series.interfaces.Serie
import kotlinx.android.synthetic.main.series_list.*
import androidx.recyclerview.widget.GridLayoutManager

abstract class BaseSeriesListFragment : BaseFragment() {

    fun showSeries(series: List<Serie>) {
        rvSeriesList.apply {
            layoutManager = GridLayoutManager(context!!, 2)
            adapter = TvSeriesAdapter(series) { item: Serie -> serieItemClicked(item) }
        }
    }

    abstract fun serieItemClicked(serie: Serie)

}