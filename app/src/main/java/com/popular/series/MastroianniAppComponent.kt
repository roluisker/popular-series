package com.popular.series

import android.content.Context
import com.popular.series.data.DataModule
import com.popular.series.ui.UiModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Component(modules = [DataModule::class, AndroidSupportInjectionModule::class, UiModule::class])
@Singleton
interface MastroianniAppComponent {

    fun inject(app: MastroianniApp)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(app: MastroianniApp): Builder

        @BindsInstance
        fun context(context: Context): Builder

        fun build(): MastroianniAppComponent
    }

}