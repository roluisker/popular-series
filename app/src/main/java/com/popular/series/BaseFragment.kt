package com.popular.series

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.popular.series.annotation.SUCCESS
import com.popular.series.net.DataResponse
import dagger.android.support.AndroidSupportInjection

abstract class BaseFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(fragmentLayout(), container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        subscribeToConsumeResponse()
    }

    private fun subscribeToConsumeResponse() {
        responseLiveData().observe(this, Observer<DataResponse> {
            consumeResponse(it)
        })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidSupportInjection.inject(this)
    }

    private fun consumeResponse(response: DataResponse?) {
        when (response!!.status) {
            SUCCESS -> {
                response.data?.let { data -> onSuccessResponse(data, response.request) }
            }
        }
    }

    protected fun navController(): NavController? {
        return view?.let { Navigation.findNavController(it) }
    }

    @LayoutRes
    abstract fun fragmentLayout(): Int

    abstract fun onSuccessResponse(data: Any?, request: String?)

    abstract fun responseLiveData(): LiveData<DataResponse>

}