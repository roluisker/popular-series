package com.popular.series.net

data class ErrorData(
    val type: String = "", val code: Int = -1,
    val message: String = "",
    val request: String = ""
) {
    override fun toString(): String {
        return if (code > 0) {
            "...>>> Error on request: ${request.toUpperCase()}, --> type: $type :{ code: $code, message: $message}"
        } else {
            "...>>> Error on request: ${request.toUpperCase()}, --> type: $type :{ message: $message} "
        }
    }
}

data class BasicError(
    val code: Int,
    val message: String
)