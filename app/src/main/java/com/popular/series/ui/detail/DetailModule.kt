package com.popular.series.ui.detail

import com.popular.series.di.InjectViewModel
import com.popular.series.di.ProvideViewModel
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module(includes = [ProvideViewModel::class])
abstract class DetailModule {
    @ContributesAndroidInjector(modules = [InjectViewModel::class])
    abstract fun bind(): DetailFragment
}