package com.popular.series.ui.popular

import android.os.Bundle
import android.view.View
import androidx.lifecycle.LiveData
import com.popular.series.BaseSeriesListFragment
import com.popular.series.R
import com.popular.series.annotation.POPULAR_SERIES
import com.popular.series.interfaces.Serie
import com.popular.series.net.DataResponse
import timber.log.Timber
import javax.inject.Inject

class PopularFragment : BaseSeriesListFragment() {

    @Inject
    lateinit var popularViewModel: PopularViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        popularViewModel.popularSeries()
    }

    override fun onSuccessResponse(data: Any?, request: String?) {
        request?.let {
            when (it) {
                POPULAR_SERIES -> showSeries(data as List<Serie>)
                else -> Timber.i("unchecked response")
            }
        }
    }

    override fun serieItemClicked(popularSerie: Serie) {
        var currentName = getString(R.string.detail)
        if (!popularSerie.name.isNullOrEmpty()) {
            currentName = popularSerie.name!!
        }
        navController()!!.navigate(PopularFragmentDirections.openDetailAction(popularSerie.id, currentName))
    }

    override fun responseLiveData(): LiveData<DataResponse> = popularViewModel.responseLiveData

    override fun fragmentLayout(): Int = R.layout.popular_fragment

}