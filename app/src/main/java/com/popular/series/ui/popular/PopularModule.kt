package com.popular.series.ui.popular

import com.popular.series.di.InjectViewModel
import com.popular.series.di.ProvideViewModel
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module(includes = [ProvideViewModel::class])
abstract class PopularModule {
    @ContributesAndroidInjector(modules = [InjectViewModel::class])
    abstract fun bind(): PopularFragment
}