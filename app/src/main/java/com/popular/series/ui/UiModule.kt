package com.popular.series.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.popular.series.di.AppViewModelFactory
import com.popular.series.ui.detail.DetailModule
import com.popular.series.ui.popular.PopularModule
import com.popular.series.ui.toprated.TopRatedModule
import com.popular.series.ui.latest.LatestModule
import dagger.Module
import dagger.Provides
import javax.inject.Provider

@Module(includes = [PopularModule::class, DetailModule::class, TopRatedModule::class, LatestModule::class])
class UiModule {
    @Provides
    fun provideViewModelFactory(providers: Map<Class<out ViewModel>, @JvmSuppressWildcards Provider<ViewModel>>):
            ViewModelProvider.Factory = AppViewModelFactory(providers)
}