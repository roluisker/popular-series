package com.popular.series.ui.detail

import com.popular.series.BaseViewModel
import com.popular.series.annotation.DETAIL
import com.popular.series.annotation.REVIEW
import com.popular.series.annotation.VIDEO
import com.popular.series.net.DataResponse
import com.popular.series.repository.SeriesRepository

class DetailViewModel(private val seriesRepository: SeriesRepository) : BaseViewModel() {

    fun detailSerie(serieId: String) {
        addDisposable(seriesRepository.serie(serieId), DETAIL)
    }

    fun video(serideId: String) {
        addDisposable(seriesRepository.serieVideo(serideId), VIDEO)
    }

    fun reviews(serieId: String) {
        addDisposable(seriesRepository.reviews(serieId, REVIEW), REVIEW)
    }

    override fun dataResponseLoaded(response: DataResponse, tagRequest: String?) {

    }

}