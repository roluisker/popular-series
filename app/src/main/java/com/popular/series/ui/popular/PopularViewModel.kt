package com.popular.series.ui.popular

import com.popular.series.BaseViewModel
import com.popular.series.annotation.POPULAR_SERIES
import com.popular.series.net.DataResponse
import com.popular.series.repository.SeriesRepository

class PopularViewModel(private val seriesRepository: SeriesRepository) : BaseViewModel() {

    fun popularSeries() {
        addDisposable(seriesRepository.series(POPULAR_SERIES), POPULAR_SERIES)
    }

    override fun dataResponseLoaded(response: DataResponse, requestTag: String?) {
    }

}