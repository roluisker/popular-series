package com.popular.series.ui.latest

import android.os.Bundle
import android.view.View
import androidx.lifecycle.LiveData
import com.popular.series.BaseSeriesListFragment
import com.popular.series.R
import com.popular.series.annotation.LATEST_SERIES
import com.popular.series.interfaces.Serie
import com.popular.series.net.DataResponse
import timber.log.Timber
import javax.inject.Inject

class LatestFragment : BaseSeriesListFragment() {

    @Inject
    lateinit var latestViewModel: LatestViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        latestViewModel.latestSeries()
    }

    override fun onSuccessResponse(data: Any?, request: String?) {
        request?.let {
            when (it) {
                LATEST_SERIES -> showSeries(data as List<Serie>)
                else -> Timber.i("unchecked response")
            }
        }
    }

    override fun serieItemClicked(serie: Serie) {
        var currentName = getString(R.string.detail)
        if (!serie.name.isNullOrEmpty()) {
            currentName = serie.name!!
        }
        navController()!!.navigate(LatestFragmentDirections.openDetailFromUpcommingAction(serie.id, currentName))
    }

    override fun responseLiveData(): LiveData<DataResponse> = latestViewModel.responseLiveData

    override fun fragmentLayout(): Int = R.layout.upcoming_fragment

}