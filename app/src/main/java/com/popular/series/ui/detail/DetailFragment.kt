package com.popular.series.ui.detail

import android.os.Bundle
import android.view.View
import androidx.lifecycle.LiveData
import com.popular.series.BaseFragment
import com.popular.series.R
import com.popular.series.adapter.IMAGE_PATH
import com.popular.series.annotation.DETAIL
import com.popular.series.interfaces.Serie
import com.popular.series.net.DataResponse
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.detail_fragment.*
import timber.log.Timber
import javax.inject.Inject
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener

import androidx.annotation.NonNull
import androidx.recyclerview.widget.LinearLayoutManager
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.PlayerConstants
import com.popular.series.adapter.ReviewAdapter
import com.popular.series.annotation.REVIEW
import com.popular.series.annotation.VIDEO
import com.popular.series.model.Review
import com.popular.series.model.Video

class DetailFragment : BaseFragment() {

    @Inject
    lateinit var detailViewModel: DetailViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loadDetail()
        lifecycle.addObserver(youtubePlayerView)
    }

    private fun loadDetail() {
        arguments?.let {
            val serieId = DetailFragmentArgs.fromBundle(it).serieId
            detailViewModel.video(serieId)
            detailViewModel.reviews(serieId)
            detailViewModel.detailSerie(serieId)
        }
    }

    override fun onSuccessResponse(data: Any?, request: String?) {
        request?.let {
            when (it) {
                DETAIL -> showSerie(data as Serie)
                VIDEO -> playVideo((data as Video).key)
                REVIEW -> showReviews(data as List<Review>)
                else -> Timber.i("unchecked response")
            }
        }
    }

    private fun showReviews(reviews: List<Review>) {

        if (reviews.isNullOrEmpty()) {
            noReviewLabel.visibility = View.VISIBLE
        } else {
            noReviewLabel.visibility = View.GONE
            reviewSeriesList.apply {
                layoutManager = LinearLayoutManager(context!!)
                adapter = ReviewAdapter(reviews)
            }
        }

    }

    private fun playVideo(videoId: String) {

        var videoPlayListener = object : AbstractYouTubePlayerListener() {

            override fun onReady(@NonNull youTubePlayer: YouTubePlayer) {
                val videoId = videoId
                youTubePlayer.loadVideo(videoId, 0f)
            }

            override fun onStateChange(youTubePlayer: YouTubePlayer, state: PlayerConstants.PlayerState) {
                if (state == PlayerConstants.PlayerState.PLAYING) {
                    fImageSerie.visibility = View.GONE
                }
            }

        }

        youtubePlayerView.addYouTubePlayerListener(videoPlayListener)

    }

    private fun showSerie(serie: Serie) {
        Picasso.get()
            .load(IMAGE_PATH + serie.poster_path)
            .resize(200, 200)
            .centerCrop()
            .into(fImageSerie)
    }

    override fun responseLiveData(): LiveData<DataResponse> = detailViewModel.responseLiveData

    override fun fragmentLayout(): Int = R.layout.detail_fragment

}