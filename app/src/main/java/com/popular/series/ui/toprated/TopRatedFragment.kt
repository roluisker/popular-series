package com.popular.series.ui.toprated

import android.os.Bundle
import android.view.View
import androidx.lifecycle.LiveData
import com.popular.series.BaseSeriesListFragment
import com.popular.series.R
import com.popular.series.annotation.TOP_RATED_SERIES
import com.popular.series.interfaces.Serie
import com.popular.series.net.DataResponse
import timber.log.Timber
import javax.inject.Inject

class TopRatedFragment : BaseSeriesListFragment() {

    @Inject
    lateinit var topRatedViewModel: TopRatedViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        topRatedViewModel.topRatedSeries()
    }

    override fun onSuccessResponse(data: Any?, request: String?) {
        request?.let {
            when (it) {
                TOP_RATED_SERIES -> showSeries(data as List<Serie>)
                else -> Timber.i("unchecked response")
            }
        }
    }

    override fun serieItemClicked(serie: Serie) {
        var currentName = getString(R.string.detail)
        if (!serie.name.isNullOrEmpty()) {
            currentName = serie.name!!
        }
        navController()!!.navigate(TopRatedFragmentDirections.openDetailFromRatedAction(serie.id, currentName))
    }

    override fun responseLiveData(): LiveData<DataResponse> = topRatedViewModel.responseLiveData

    override fun fragmentLayout(): Int = R.layout.top_rated_fragment

}