package com.popular.series.ui.toprated

import com.popular.series.BaseViewModel
import com.popular.series.annotation.TOP_RATED_SERIES
import com.popular.series.net.DataResponse
import com.popular.series.repository.SeriesRepository

class TopRatedViewModel(private val seriesRepository: SeriesRepository) : BaseViewModel() {

    fun topRatedSeries() {
        addDisposable(seriesRepository.series(TOP_RATED_SERIES), TOP_RATED_SERIES)
    }

    override fun dataResponseLoaded(response: DataResponse, tagRequest: String?) {

    }

}