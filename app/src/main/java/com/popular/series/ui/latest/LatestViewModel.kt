package com.popular.series.ui.latest

import com.popular.series.BaseViewModel
import com.popular.series.annotation.LATEST_SERIES
import com.popular.series.annotation.POPULAR_SERIES
import com.popular.series.net.DataResponse
import com.popular.series.repository.SeriesRepository
import timber.log.Timber

class LatestViewModel(private val seriesRepository: SeriesRepository): BaseViewModel() {

    fun latestSeries() {
        addDisposable(seriesRepository.series(LATEST_SERIES), LATEST_SERIES)
    }

    override fun dataResponseLoaded(response: DataResponse, tagRequest: String?) {

    }

}