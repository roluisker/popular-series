package com.popular.series.api

import com.google.gson.JsonElement
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path
import retrofit2.http.Query

const val HEADER_ACCEPT_JSON: String = "Accept: application/json"
const val HEADER_CONTENT_TYPE_JSON: String = "Content-Type: application/json"

interface TvApi {

    @Headers(HEADER_ACCEPT_JSON, HEADER_CONTENT_TYPE_JSON)
    @GET("popular")
    fun getPopularTvSeries(
        @Query("page") page: String = "1"
    ): Observable<JsonElement>

    @Headers(HEADER_ACCEPT_JSON, HEADER_CONTENT_TYPE_JSON)
    @GET("top_rated")
    fun getTopRatedTvSeries(
        @Query("page") page: String = "1"
    ): Observable<JsonElement>

    @Headers(HEADER_ACCEPT_JSON, HEADER_CONTENT_TYPE_JSON)
    @GET("on_the_air")
    fun getLatestSeries(
        @Query("page") page: String = "1"
    ): Observable<JsonElement>

    @Headers(HEADER_ACCEPT_JSON, HEADER_CONTENT_TYPE_JSON)
    @GET("{tv_id}")
    fun getSerie(
        @Path("tv_id") serieId: String
    ): Observable<JsonElement>

    @Headers(HEADER_ACCEPT_JSON, HEADER_CONTENT_TYPE_JSON)
    @GET("{tv_id}/videos")
    fun getVideos(
        @Path("tv_id") serieId: String
    ): Observable<JsonElement>

    @Headers(HEADER_ACCEPT_JSON, HEADER_CONTENT_TYPE_JSON)
    @GET("{tv_id}/reviews")
    fun getReviews(
        @Path("tv_id") serieId: String
    ): Observable<JsonElement>

}