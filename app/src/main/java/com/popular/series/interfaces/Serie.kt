package com.popular.series.interfaces

interface Serie {
    var id: String
    var name: String?
    var description: String?
    var poster_path: String?
}