package com.popular.series.data

import android.content.Context
import androidx.room.Room
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.popular.series.api.TvApi
import com.popular.series.db.ReviewDao
import com.popular.series.db.SerieDao
import com.popular.series.db.TvSeriesDataBase
import com.popular.series.repository.SeriesRepositoryImpl
import com.popular.series.repository.SeriesRepository
import dagger.Module
import dagger.Provides
import dagger.Reusable
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class DataModule {

    @Provides
    @Reusable
    fun provideGson(): Gson {
        val builder = GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
        return builder.setLenient().create()
    }

    @Provides
    @Reusable
    fun provideOkHttpClient(interceptor: Interceptor): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(interceptor)
            .connectTimeout(10, TimeUnit.SECONDS)
            .writeTimeout(10, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .build()
    }

    @Provides
    @Reusable
    fun provideMovieApi(retrofit: Retrofit): TvApi = retrofit.create(TvApi::class.java)

    @Provides
    @Reusable
    fun provideRetrofit(gson: Gson, okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .baseUrl("https://api.themoviedb.org/3/tv/")
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }

    @Provides
    @Reusable
    fun provideInterceptor(): Interceptor {
        return Interceptor { chain ->
            val newUrl = chain.request().url()
                .newBuilder()
                .addQueryParameter("api_key", "b07c7f482d80a6d112aa982503b1b7b5")
                .addQueryParameter("language", "en-US")
                .build()
            val newRequest = chain.request()
                .newBuilder()
                .url(newUrl)
                .build()
            chain.proceed(newRequest)
        }
    }

    @Provides
    @Singleton
    fun providePopularRepository(serieDao: SerieDao, reviewDao: ReviewDao, serieApi: TvApi): SeriesRepository =
        SeriesRepositoryImpl(serieApi, serieDao, reviewDao)

    @Provides
    @Singleton
    fun provideTvSeriesDatabase(context: Context) =
        Room.databaseBuilder(context, TvSeriesDataBase::class.java, "tv_mastroianni.db")
            .fallbackToDestructiveMigration()
            .build()

    @Provides
    @Singleton
    fun provideSerieDao(db: TvSeriesDataBase): SerieDao = db.serieDao()

    @Provides
    @Singleton
    fun provideReviewDao(db: TvSeriesDataBase): ReviewDao = db.reviewDao()

}