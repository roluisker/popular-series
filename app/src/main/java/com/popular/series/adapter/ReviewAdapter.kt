package com.popular.series.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.popular.series.R
import com.popular.series.model.Review
import kotlinx.android.synthetic.main.review_item.view.*

class ReviewAdapter(val reviews: List<Review>) : RecyclerView.Adapter<ReviewAdapter.ReviewViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):
            ReviewViewHolder = ReviewViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.review_item, parent, false)
    )

    override fun onBindViewHolder(holder: ReviewViewHolder, position: Int) {
        holder.bind(reviews[position])
    }

    class ReviewViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(review: Review) = with(itemView) {
            reviewTitle.text = review.author
            reviewDescription.text = review.content
        }

    }

    override fun getItemCount() = reviews.size

}