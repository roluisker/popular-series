package com.popular.series.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.popular.series.R
import com.popular.series.image.RoundedCornersTransform
import com.popular.series.interfaces.Serie
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.serie_list_item.view.*

const val IMAGE_PATH = "https://image.tmdb.org/t/p/w500"

class TvSeriesAdapter(
    val series: List<Serie>,
    private val serieClickListener: (Serie) -> Unit
) :
    RecyclerView.Adapter<TvSeriesAdapter.SerieViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):
            SerieViewHolder = SerieViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.serie_list_item, parent, false)
    )

    override fun onBindViewHolder(holder: SerieViewHolder, position: Int) {
        holder.bind(series[position], serieClickListener)
    }

    class SerieViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(serie: Serie, clickListener: (Serie) -> Unit) = with(itemView) {
            imageSerie?.let {
                Picasso.get()
                    .load(IMAGE_PATH + serie.poster_path)
                    .resize(200, 200)
                    .transform(RoundedCornersTransform())
                    //.placeholder(R.drawable.ic_holder)
                    .centerCrop()
                    .into(it)
            }
            itemView.setOnClickListener { clickListener(serie) }
        }

    }

    override fun getItemCount() = series.size

}