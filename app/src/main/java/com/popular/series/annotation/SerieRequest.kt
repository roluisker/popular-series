package com.popular.series.annotation

import androidx.annotation.StringDef

const val POPULAR_SERIES: String = "popular_series"
const val TOP_RATED_SERIES: String = "top_rated_series"
const val LATEST_SERIES: String = "latest_series"
const val DETAIL: String = "detail"
const val VIDEO: String = "video"
const val REVIEW: String = "review"

@Retention(AnnotationRetention.SOURCE)
@StringDef(POPULAR_SERIES, TOP_RATED_SERIES, LATEST_SERIES, DETAIL, VIDEO, REVIEW)
annotation class SerieRequest