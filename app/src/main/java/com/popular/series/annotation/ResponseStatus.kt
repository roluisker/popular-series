package com.popular.series.annotation

import androidx.annotation.StringDef

const val ERROR: String = "data_error"
const val SUCCESS: String = "data_success"
const val COMPLETED: String = "data_completed"
const val LOADING: String = "data_loading"

@Retention(AnnotationRetention.SOURCE)
@StringDef(SUCCESS, LOADING, COMPLETED, ERROR)
annotation class ResponseStatus