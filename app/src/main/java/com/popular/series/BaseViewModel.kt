package com.popular.series

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.popular.series.net.DataResponse
import com.popular.series.net.ErrorData
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

open abstract class BaseViewModel : ViewModel() {

    private val disposables = CompositeDisposable()

    val responseLiveData = MutableLiveData<DataResponse>()

    protected open fun <T : Any> addDisposable(observable: Observable<T>, request: String?) {
        disposables.add(observable.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseLiveData.postValue(DataResponse.loading())
            }
            .subscribe({
                val dataResponse = DataResponse.success(it, request)
                dataResponseLoaded(dataResponse, request)
                responseLiveData.postValue(dataResponse)
            }, {
                val error = parseErrorResponse(it, request ?: "")
                Timber.e(it, error.toString())
                responseLiveData.postValue(DataResponse.error(error, request))
            })
        )
    }

    private fun parseErrorResponse(throwable: Throwable, request: String): ErrorData {
        return ErrorData("UNEXPECTED", message = throwable.message ?: "", request = request)
    }

    override fun onCleared() {
        disposables.clear()
    }

    abstract fun dataResponseLoaded(response: DataResponse, tagRequest: String?)

}