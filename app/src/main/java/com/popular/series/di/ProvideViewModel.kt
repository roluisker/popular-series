package com.popular.series.di

import androidx.lifecycle.ViewModel
import com.popular.series.annotation.ViewModelKey
import com.popular.series.repository.SeriesRepository
import com.popular.series.ui.detail.DetailViewModel
import com.popular.series.ui.popular.PopularViewModel
import com.popular.series.ui.toprated.TopRatedViewModel
import com.popular.series.ui.latest.LatestViewModel
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap

@Module
class ProvideViewModel {

    @Provides
    @IntoMap
    @ViewModelKey(PopularViewModel::class)
    fun providePopularViewModel(seriesSeriesRepository: SeriesRepository): ViewModel =
        PopularViewModel(seriesSeriesRepository)

    @Provides
    @IntoMap
    @ViewModelKey(DetailViewModel::class)
    fun provideDetailViewModel(seriesSeriesRepository: SeriesRepository): ViewModel =
        DetailViewModel(seriesSeriesRepository)

    @Provides
    @IntoMap
    @ViewModelKey(TopRatedViewModel::class)
    fun provideTopRatedViewModel(seriesSeriesRepository: SeriesRepository): ViewModel =
        TopRatedViewModel(seriesSeriesRepository)

    @Provides
    @IntoMap
    @ViewModelKey(LatestViewModel::class)
    fun provideUpcomingViewModel(seriesSeriesRepository: SeriesRepository): ViewModel =
        LatestViewModel(seriesSeriesRepository)

}