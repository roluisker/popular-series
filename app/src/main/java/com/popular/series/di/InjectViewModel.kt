package com.popular.series.di

import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.popular.series.ui.detail.DetailFragment
import com.popular.series.ui.detail.DetailViewModel
import com.popular.series.ui.popular.PopularFragment
import com.popular.series.ui.popular.PopularViewModel
import com.popular.series.ui.toprated.TopRatedFragment
import com.popular.series.ui.toprated.TopRatedViewModel
import com.popular.series.ui.latest.LatestFragment
import com.popular.series.ui.latest.LatestViewModel
import dagger.Module
import dagger.Provides

@Module
class InjectViewModel {

    @Provides
    fun providePopularViewModel(
        factory: ViewModelProvider.Factory,
        target: PopularFragment
    ): PopularViewModel =
        ViewModelProviders.of(target, factory).get(PopularViewModel::class.java)

    @Provides
    fun provideDetailViewModel(
        factory: ViewModelProvider.Factory,
        target: DetailFragment
    ): DetailViewModel =
        ViewModelProviders.of(target, factory).get(DetailViewModel::class.java)

    @Provides
    fun provideTopRatedViewModel(
        factory: ViewModelProvider.Factory,
        target: TopRatedFragment
    ): TopRatedViewModel =
        ViewModelProviders.of(target, factory).get(TopRatedViewModel::class.java)

    @Provides
    fun provideUpcomingViewModel(
        factory: ViewModelProvider.Factory,
        target: LatestFragment
    ): LatestViewModel =
        ViewModelProviders.of(target, factory).get(LatestViewModel::class.java)

}