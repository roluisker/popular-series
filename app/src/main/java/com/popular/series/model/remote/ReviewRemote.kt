package com.popular.series.model.remote

import com.popular.series.model.Review

data class ReviewRemote(
    var page: Int,
    var id: Int,
    var results: List<Review> = emptyList()
)