package com.popular.series.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.popular.series.interfaces.Serie

@Entity(tableName = "tvserie")
data class TvSerie(
    override var name: String? = "",
    override var description: String? = "",
    override var poster_path: String? = "",
    @PrimaryKey
    override var id: String,
    var tag: String? = ""
) : Serie