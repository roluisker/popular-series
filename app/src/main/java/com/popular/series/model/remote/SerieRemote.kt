package com.popular.series.model.remote

import com.popular.series.model.TvSerie

data class SerieRemote(
    var page: Int,
    var totalResults: Int,
    var totalPages: Int,
    var results: List<TvSerie> = emptyList()
)