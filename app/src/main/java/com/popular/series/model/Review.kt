package com.popular.series.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "review")
data class Review(
    var author: String,
    var content: String,
    @PrimaryKey
    var id: String,
    var tag: String? = ""
)

