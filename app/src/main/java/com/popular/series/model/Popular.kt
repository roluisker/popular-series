package com.popular.series.model

import com.popular.series.interfaces.Serie

data class Popular(
    override var name: String? = "",
    override var description: String? = "",
    override var poster_path: String? ="",
    override var id: String
) : Serie