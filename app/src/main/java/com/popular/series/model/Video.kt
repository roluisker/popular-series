package com.popular.series.model

data class Video(var id: String, var key: String, var type: String)