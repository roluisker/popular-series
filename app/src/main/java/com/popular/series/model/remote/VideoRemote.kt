package com.popular.series.model.remote

import com.popular.series.model.Video

data class VideoRemote(
    var id: String,
    var results: List<Video>
)