package com.popular.series.util

import androidx.recyclerview.widget.DiffUtil
import com.popular.series.interfaces.Serie

class PostsDiffUtilCallback(private val oldList: List<Serie>, private val newList: List<Serie>) : DiffUtil.Callback() {
    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
        oldList[oldItemPosition].id == newList[newItemPosition].id

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) = true
}