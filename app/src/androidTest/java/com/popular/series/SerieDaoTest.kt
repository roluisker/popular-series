package com.popular.series

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.After
import java.io.IOException

import com.popular.series.db.SerieDao
import com.popular.series.db.TvSeriesDataBase
import com.popular.series.model.TvSerie
import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class SerieDaoTest {

    private lateinit var serieDao: SerieDao
    private lateinit var serieDatabase: TvSeriesDataBase
    private val serie = TvSerie("Serie1", "serie 1 description", "image_path", "32424")

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        serieDatabase = Room.inMemoryDatabaseBuilder(
            context, TvSeriesDataBase::class.java
        ).build()
        serieDao = serieDatabase.serieDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        serieDatabase.close()
    }

    @Test
    @Throws(Exception::class)
    fun writeTvSeriesAndDeleteTest() {

        serieDao.insertSerie(serie)
        var result = serieDao.series()!!

        result.map {
            MatcherAssert.assertThat(it[0].id, CoreMatchers.equalTo(serie.id))
        }

        serieDao.dropSeries()

        var dropResult = serieDao.series()!!

        dropResult.map {
            assert(it.isEmpty())
        }

    }

    @Test
    @Throws(Exception::class)
    fun addTvSeriesTest() {

        serieDao.insertSerie(serie)
        serieDao.insertSerie(serie)

        var result = serieDao.series()!!

        result.map {
            assert(it.size == 2)
        }

    }

}